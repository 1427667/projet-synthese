// Lecture de JSON et attribution des informations du fichier .json dans une liste.
$(document).ready(function(){
    $("#pourEmploi").hide();
    $("#dEmploi").hide();

    $.getJSON("etudiants.json", function (data) {
        let etudiants = []; // Liste contenant les informations des étudiants.
        $.each(data, function (key, val) {
            // key = étudiant || val = informations sur l'étudiant.
            etudiants[key] = val;
        });
        console.log(etudiants);
        remplirTableauEtudiant(etudiants) // Envoie de la liste vers la fonction qui remplit le tableau.
    });

    $.getJSON("DossierPourEmploi.json", function (data) {
        let emploi = []; // Liste contenant les informations des emplois.
        $.each(data, function (key, val) {
            // key = emploi || val = informations sur l'emploi.
            emploi[key] = val;
        });
        console.log(emploi);
        remplirTableauPourEmploi(emploi) // Envoie de la liste vers la fonction qui remplit le tableau.
    });
    $.getJSON("DossierEmploi.json", function(data){
        let dossieremploi = [];
        $.each(data, function (key, val){
            dossieremploi[key] = val;
        });
        remplirTableauDEmploi(dossieremploi);
    })
});

// Remplissage du tableau 'Étudiants' ('table#etudiants' dans 'gestionEtudiants.html') avec les informations venant du fichier .json et du localStorage. 
function remplirTableauEtudiant(etudiants) {

    // Remplissage avec les informations du fichier .json.
    let count = 0;
    for (let i of etudiants){
        $("#etudiants").append(
            "<tr>" +
                "<td>" + etudiants[count].NoEtu + "</td>" +
                "<td>" + etudiants[count].Nom + "</td>" +
                "<td>" + etudiants[count].Prenom + "</td>" +
                "<td>" + etudiants[count].Genre + "</td>" +
                "<td>" + etudiants[count].Programme + "</td>" +
                "<td>" + etudiants[count].Particularite + "</td>" +
                "<td>" + etudiants[count].Citoyennete + "</td>" +
                "<td>" + etudiants[count].EtatEtudiant + "</td>" +
                "<td>" + etudiants[count].Courriel + "</td>" +
                "<td>" + etudiants[count].NoTel + "</td>" +
                "<td>" + etudiants[count].NoCel + "</td>" +
            "</tr>"
        );
        count++;
    }

    // Ajout des informations du localStorage s'il y en a.
    if(localStorage.length !== 0){

        /* 
        On utilise la longueur de la liste pour trouver les clés.
        'i' est l'index des clés. Elle nous permet de trouver les chiffres exacts de la clé. 
        */
        for(let i=0; i<localStorage.length; i++) {
            // La clé est trouver et temporairement mise dans une variable.
            let key = localStorage.key(i); 
            // La clé est utilisé pour trouver la liste d'informations qui est par la suite convertie de 'String' à 'Array' via 'JSON.parse()'.
            let listLocalStorage = JSON.parse(localStorage.getItem(key));

            /* 
            Une fois que la liste est de type Array, on peut l'utiliser comme une Array normal.
            On utilise la table qui contient le tbody '#etudiants' et on y ajoute une rangée (<tr></tr>) 
            avec des colonnes (<td></td>) contenant les informations de 'listLocalStorage'.
            */
            $("#etudiants").append(
                "<tr>" +
                    "<td>" + listLocalStorage.NoEtu + "</td>" +
                    "<td>" + listLocalStorage.Nom + "</td>" +
                    "<td>" + listLocalStorage.Prenom + "</td>" +
                    "<td>" + listLocalStorage.Genre + "</td>" +
                    "<td>" + listLocalStorage.Programme + "</td>" +
                    "<td>" + listLocalStorage.Particularite + "</td>" +
                    "<td>" + listLocalStorage.Citoyennete + "</td>" +
                    "<td>" + listLocalStorage.EtatEtudiant + "</td>" +
                    "<td>" + listLocalStorage.Courriel + "</td>" +
                    "<td>" + listLocalStorage.NoTel + "</td>" +
                    "<td>" + listLocalStorage.NoCel + "</td>" +
                "</tr>"
            );
        }
    }
}

function remplirTableauPourEmploi(p){
    // Remplissage avec les informations du fichier .json.
    let count = 0;
    for (let i of p){
        $("#pourEmploi").append(
            "<tr>" +
                "<td>" + p[count].CoteSecurite + "</td>" +
                "<td>" + p[count].Employeur + "</td>" +
                "<td>" + p[count].DossierJudiciaire + "</td>" +
                "<td>" + p[count].Preference + "</td>" +
                "<td>" + p[count].NiveauAnglais + "</td>" +
                "<td>" + p[count].NiveauFrancais + "</td>" +
                "<td>" + p[count].Automobile + "</td>" +
            "</tr>"
        );
        count++;
    }
}

function remplirTableauDEmploi(p){
    let count = 0;
    for (let i of p){
        $("#dEmploi").append(
            "<tr>" +
            "<td>" + p[count].MilieuEmploi + "</td>" +
            "<td>" + p[count].Debut + "</td>" +
            "<td>" + p[count].Fin + "</td>" +
            "<td>" + p[count].Poste + "</td>" +
            "<td>" + p[count].NomResponsable + "</td>" +
            "<td>" + p[count].Info + "</td>" +
            "</tr>"
        );
        count++;
    }
}

$(".tableau-button").on("click", function(){
    $(".tableau-button").removeClass("active");
    switch($(this).text()){
        case "Etudiants":
            $(this).addClass("active"); // Rend le nav-tab actif.
            // Affiche le tableau demandé et cache les autres.
            $("#dEmploi").hide();
            $("#pourEmploi").hide();
            $("#etudiants").show();
            break;
        case "Dossier pour emploi":
            $(this).addClass("active"); // Rend le nav-tab actif.
            // Affiche le tableau demandé et cache les autres.
            $("#etudiants").hide();
            $("#dEmploi").hide();
            $("#pourEmploi").show();

            // Fonction qui remplit tableau avec les bonnes informations.
            pourEmploiTableau();
            break;
        case "Dossier d'emploi":
            $(this).addClass("active"); // Rend le nav-tab actif.
            // Affiche le tableau demandé et cache les autres.
            $("#etudiants").hide();
            $("#pourEmploi").hide();
            $("#dEmploi").show();

            // Fonction qui remplit tableau avec les bonnes informations.
            dEmploiTableau();
            break;
    }
});

// Création du étudiant via 'creationEtudiant.html' et sauvegarde des données dans le localStorage.
function creerEtudiant(){
    /*
    L'Array 'etudiant' contient tous les valeurs entrées dans le '<form>' de 'creationEtudiant.html'.
    Par la suite, 'etudiant' est changé en string et sauvegardé dans le localStorage, avec le numéro de l'étudiant comme clé.
    */
    let etudiant = {
        NoEtu: $("input[name=NoEtu]").val(),
        Nom: $("input[name=Nom]").val(),
        Prenom: $("input[name=Prenom]").val(),
        Genre: $("input[name=Genre]:checked").val(),
        Programme: $("input[name=Programme]").val(),
        Particularite: $("textarea[name=Particularite]").val(),
        Citoyennete: $("input[name=citoyen]:checked").val(),
        EtatEtudiant: $("#etat").children("option:selected").val(),
        Courriel: $("input[type=email]").val(),
        NoTel: $("input[name=telephone]").val(),
        NoCel: $("input[name=cell]").val()
    };
    localStorage.setItem(etudiant.NoEtu, JSON.stringify(etudiant));
}

/*
// Fonction qui permet d'éffacer les elements du tableau '#etudiants'.
function viderTableau(){
    $("#etudiants thead, #etudiants tbody").empty();
}
*/

function keepUser(){
    sessionStorage.setItem("user", $("#user").val());

    const userInfo = $("#userinfo");
    userInfo.append(sessionStorage.getItem("user"));
}

function clearLocalStorage(){
    localStorage.clear();
    location.reload();
}
